<?php
/*
Copyright (c) 2016-2017 ZettaScript, Pascal Engélibert
This file is part of ATTCHAR.

	ATTCHAR is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ATTCHAR is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with ATTCHAR.  If not, see <http://www.gnu.org/licenses/>.
*/

$attchar_ok = false;
if(strlen($_POST['attchar_code']) != 8)
	exit();
require_once('database.php');
$req = $attchar_bdd->prepare('SELECT `code` FROM `attchar` WHERE `hashcode` = ? AND `expire` > ? AND `haship` = ?');
$req->execute(array($_POST['attchar_auto'], time(), sha1($_SERVER['REMOTE_ADDR'])));
if($data = $req->fetch()) {
	$ucode = strtoupper($_POST['attchar_code']);
	$difs = 0;
	for($i=0; $i<8; $i++) {// Count number of errors
		if($ucode[$i] != $data['code'][$i])
			$difs ++;
	}
	if($difs <= 1)// Allow 1 error
		$attchar_ok = true;
}
$req->closeCursor();
?>
